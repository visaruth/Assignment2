package com.example.megas.assignment2;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    Calendar myCalendar = Calendar.getInstance();
    public static final String PREFS_NAME = "ProfileLog";
    SimpleDateFormat fmtDateAndTime = new SimpleDateFormat("dd-MMM-yyyy");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = (Spinner) findViewById(R.id.kumnumnar);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this,
                R.array.kumnumnar, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            ((EditText)findViewById(R.id.editText3)).setText(fmtDateAndTime.format(myCalendar.getTime()));
        }
    };

    public void setDate(View v){
        DatePickerDialog dateP = new DatePickerDialog(MainActivity.this, d,
        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
        myCalendar.get(Calendar.DAY_OF_MONTH));
        dateP.show();

    }



    public void submitData(View v){
        boolean check = true;
        Intent nextActivity = new Intent(MainActivity.this, MainActivity2.class);
        String name = (String)((EditText)findViewById(R.id.editText)).getText().toString();
        String lname = (String)((EditText)findViewById(R.id.editText2)).getText().toString();
        String bDate = (String)((EditText)findViewById(R.id.editText3)).getText().toString();
        String email = (String)((EditText)findViewById(R.id.editText4)).getText().toString();
        String pnum = (String)((EditText)findViewById(R.id.editText5)).getText().toString();

        if(name.equalsIgnoreCase("")||lname.equalsIgnoreCase("")||bDate.equalsIgnoreCase("")||email.equalsIgnoreCase("")||pnum.equalsIgnoreCase("")){
            Toast.makeText(MainActivity.this,"Please complete all editText.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!name.matches("[A-Z]+[a-z]*")&&check){
            Toast.makeText(MainActivity.this,"The first character of name is upper case.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!lname.matches("[A-Z]+[a-z]*")&&check){
            Toast.makeText(MainActivity.this,"The first character of lastname is upper case.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!email.matches("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)")&&check){
            Toast.makeText(MainActivity.this,"Invalid email.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(!(pnum.length()==10||pnum.length()==9)){
            Toast.makeText(MainActivity.this,"The telephone number should have 10 or 9 numbers.", Toast.LENGTH_SHORT).show();
            check = false;
        }

        if(check){
            String[] parts = bDate.split("-");
            int age = Calendar.getInstance().get(Calendar.YEAR)-Integer.valueOf(parts[2]);

            RadioGroup radioButtonGroup = (RadioGroup)findViewById(R.id.file_type);
            int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
            View radioButton = radioButtonGroup.findViewById(radioButtonID);
            int idx = radioButtonGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  radioButtonGroup.getChildAt(idx);
            String selectedtext = r.getText().toString();

            if(selectedtext.equalsIgnoreCase("Pref. Obj.")){
                SharedPreferences shared_pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = shared_pref.edit();
                editor.putString("name", (String)((Spinner)findViewById(R.id.kumnumnar)).getSelectedItem().toString()+" "+name);
                editor.putString("lname", lname);
                editor.putString("age", ""+age);
                editor.putString("email", email);
                editor.putString("pnum", pnum);
            }

            if(selectedtext.equalsIgnoreCase("File")){
                String filename = "ProfileLog.txt";
                String string = "name- "+(String)((Spinner)findViewById(R.id.kumnumnar)).getSelectedItem().toString()+" "+name+"_lname-"+lname+"_age-"+age+"_email-"+email+"_pnum-"+pnum;
                FileOutputStream outputStream;
                try {
                    outputStream = openFileOutput(filename, MODE_PRIVATE);
                    outputStream.write(string.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            nextActivity.putExtra("name", (String)((Spinner)findViewById(R.id.kumnumnar)).getSelectedItem().toString()+" "+name);
            nextActivity.putExtra("lname", lname);
            nextActivity.putExtra("age", ""+age);
            nextActivity.putExtra("email", email);
            nextActivity.putExtra("pnum", pnum);
            startActivity(nextActivity);
        }
    }
}
